import Image from "next/image";
import Albums from "./albums/page"

export default function Home() {
  return (
    <>
    <header>
      <div className="container mx-auto">
        <div className="header grid grid-rows-1 grid-flow-col gap-4">
          <div className="logo">
            <a href="#" className="flex items-center gap-x-0.5 h-11 lg:h-12">
              <img src="images/logo-1.svg" alt=""  className="h-[75%]"/>
              <img src="images/logo-2.svg" alt="" className="h-[30%]"/>
              <img src="images/logo-3.svg" alt="" className="h-full"/>
            </a>
          </div>
          <div className="nav flex items-center">
            <ul className="flex items-center">
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">Guests</a>
              </li>
              <li>
                <a href="#">Rundown</a>
              </li>
              <li>
                <a href="#">Tenants</a>
              </li>
              <li>
                <a href="#">Promo</a>
              </li>
              <li>
                <a href="#">Gallery</a>
              </li>
              <li>
                <a href="#">More</a>
              </li>
            </ul>
          </div>
          <div className="btn-ticket ml-auto flex items-center flex-shrink-0 gap-x-1 sm:gap-x-2">
            <a href="#" className="flex items-center justify-center px-3 py-2.5 lg:px-4 lg:py-3.5 font-bold tracking-tight rounded-full transition-colors duration-300 border bg-accent-yellow-base text-black hover:bg-accent-yellow-darken border-transparent">
              Get your ticket
            </a>
          </div>
        </div>
      </div>
    </header>
    <div className="main-page">
      <div className="bg-main absolute inset-0 z-0">
        <img src="images/bg-pattern.svg" alt="" className="w-full object-cover opacity-20" />
      </div>
      <div className="absolute inset-0 z-10 bg-gradient-to-b"></div>
      <div className="container mx-auto">
        <div className="banner z-99 relative">
          <div className="banner-hero gap-5">
            <div className="text-hero pt-10">            
              <h4 className="hero-title-small text-base uppercase">Indonesia Comic Con 2023 ⚡️</h4>
              <h2 className="hero-title-big uppercase text-[4vw] !leading-[1.1] mt-3">Bringing <br/>the Best<br/> <span className="text-orange-700">Pop</span> <span className="text-amber-300">Culture</span><br/>Experience</h2>
              <div className="btn-banner flex mt-5">
                <a href="#" className="items-center justify-center px-3 py-2.5 lg:px-4 lg:py-3.5 font-bold rounded-full border bg-yellow-400 text-black border-transparent">Get your ticket</a>
                <a href="#" className="items-center justify-center px-3 py-2.5 lg:px-4 lg:py-3.5 font-bold rounded-full border bg-transparent text-white border-white ml-5">Br part of ICC</a>
              </div>
            </div>
            <div className="red-hero flex flex-col items-center justify-center bg-red-600 bg-gradient-to-t from-red-600 to-red-800 rounded-2xl px-6 py-6 h-full">
              <h3 className="font-barlow text-center text-[34px] font-black tracking-tighter">Nox 4-5, 2023</h3>
              <p className="text-center mt-3">Jakarta Convention Center (JCC) Senayan, Assembly, Cendrawasih, and Plenary Hall</p>
              <p className="text-center mt-4">10 AM - 9 PM</p>
            </div>
            <div className="yellow-hero p-10 flex flex-col text-slate-950 items-center justify-center bg-amber-400 rounded-2xl h-full">
              <p>The biggest event in the year to celebrate</p>
              <h3 className="font-barlow text-center text-[34px] font-black tracking-tighter">5 POP CULTURE PILLARS</h3>
            </div>
          </div>
        </div>
        <div className="news-area z-20 relative grid grid-cols-2 mt-5 gap-5">
          <div className="img-news rounded-2xl overflow-hidden">
            <img src="../images/hero-gallery-01.webp" alt="" className="object-cover relative h-full" />
          </div>
          <div className="text-news">
            <div className="rounded-3xl bg-gradient-to-r from-red-700 via-red-500 to-black p-1 mb-3">
              <div className=" h-full w-full bg-black rounded-3xl p-5">
                <h3 className="font-barlow font-black text-[24px] tracking-tighter">DG Awards 2023</h3>
                <p className="text-sm">The biggest gaming award event in Indonesia is coming.</p>
                <a href="#">Link</a>
              </div>
            </div>
            <div className="rounded-3xl bg-gradient-to-r from-amber-300 via-amber-300 to-black p-1 mb-3">
              <div className=" h-full w-full bg-black rounded-3xl p-5">
                <h3 className="font-barlow font-black text-[24px] tracking-tighter">Championship of Cosplay</h3>
                <p className="text-sm">Unleash your inner hero at Cosplay Competition! Join the fun and win a total prize of Rp50,000,000.</p>
                <a href="#">Link</a>
              </div>
            </div>
            <div className="rounded-3xl bg-gradient-to-r from-cyan-400 via-cyan-400 to-black p-1 mb-3">
              <div className=" h-full w-full bg-black rounded-3xl p-5">
                <h3 className="font-barlow font-black text-[24px] tracking-tighter">FanMate Fest</h3>
                <p className="text-sm">Get a chance to win a huge prize and have a chance to be the next Cosplay Idol, Sign-up FanMate account and register as a creator!</p>
                <a href="#">Link</a>
              </div>
            </div>
          </div>
        </div>
        <Albums />
      </div>
    </div>
    </>
  );
}
