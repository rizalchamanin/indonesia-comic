import React from 'react'

const Card = ({ img }) => {
  return (
    <div className='w-[100%] bg-red-600 shadow-xl mb-3'>
        <div>
            <img src={img} alt="" className='h-[250px] w-[100%] object-cover' />
            <div className='flex flex-col gap-4 p-[20px]'>
                <div>
                    <h3 className='font-bold'>iki team</h3>
                </div>
                <div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium nihil harum fuga hic, inventore blanditiis, itaque officiis quo doloribus, quibusdam distinctio est explicabo iusto? Illo hic illum perferendis non repellendus.</p>
                </div>
                <div>
                    <a href="#" className='text-black'>Baca dong</a>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Card