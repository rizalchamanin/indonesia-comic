'use client'
import React from 'react'
import Card from './Card'
import Slider from "react-slick";

const data = [
    { img : "./images/vierratale.01bd270.webp" },
    { img : "./images/ayunda-risu.ab5cb8e.webp" },
    { img : "./images/dj-haruka.bdb27cf.webp" },
    { img : "./images/ely.80e985b.webp" },
    { img : "./images/hakaosan.6397957.webp" },
    { img : "./images/larissa.4c558b7.webp" },
];

const Albums = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
    };
    return (
        <div className='mt-5 relative z-10'>
            <Slider {...settings}>
                {data.map((el, index) => <Card key={index} img={el.img} />)}
            </Slider>
        </div>
    )
}

export default Albums
